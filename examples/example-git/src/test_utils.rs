use git2::Repository;
use std::fs;
use std::path::PathBuf;

pub fn write_file(path: &str, content: &str) -> anyhow::Result<()> {
    let buf = PathBuf::from(path);
    let parent = buf.parent().unwrap();
    fs::create_dir_all(parent)?;
    fs::write(path, content)?;
    Ok(())
}
pub fn setup() -> anyhow::Result<String> {
    let uuid = uuid::Uuid::new_v4().to_string();
    let mut buf = PathBuf::new();
    buf.push("test_folder");
    buf.push(&uuid);
    let folder = buf.to_str().unwrap();
    fs::create_dir_all(folder)?;
    let _repo = Repository::init(folder)?;
    let exists_git_folder = fs::exists(folder.to_owned() + "/.git")?;
    assert!(exists_git_folder);
    Ok(folder.to_string())
}
pub fn tear_down() -> anyhow::Result<()> {
    fs::remove_dir_all("test_folder")?;
    Ok(())
}
