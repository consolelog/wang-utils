mod test_utils;

use crate::test_utils::{setup, tear_down, write_file};
use std::env;
use tracing::info;
use wang_utils::git::{
    git_add, git_commit, git_init, git_read_commits, git_read_tag, git_status, git_tag,
};
use wang_utils::logger::init_tracing;

fn main() -> anyhow::Result<()> {
    env::set_var("RUST_LOG", "info");
    // 测试logger包
    init_tracing();
    // 初始化测试目录
    let test_folder = setup()?;
    //git init
    let repo = git_init(&test_folder)?;
    // 输出文件
    let path = format!("{test_folder}/test1/test2/test.txt");
    write_file(&path, "111")?;
    git_add(&repo)?; //git add .
    info!("status:{:?}", serde_json::to_string(&git_status(&repo)?)?);
    git_commit(&repo, "initial commit")?; //git commit -m "xxx"
    info!("status:{:?}", serde_json::to_string(&git_status(&repo)?)?);
    info!("commits:{:?}", git_read_commits(&repo)?);
    info!("tags:{:?}", git_read_tag(&repo)?);
    git_tag(&repo, "v1.2.14")?;
    info!("tags:{:?}", git_read_tag(&repo)?);
    info!("commits:{:?}", git_read_commits(&repo)?);
    // 删除测试目录
    tear_down()?;
    Ok(())
}
