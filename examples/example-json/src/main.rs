use serde_json::json;
use std::fs;
use wang_utils::json::write_pretty_json_to_file;

fn main() -> anyhow::Result<()> {
    let json = json!({
        "user":"123312",
        "pwd":"user-pwd"
    });
    println!("json:{json:?}");
    write_pretty_json_to_file("test.json", &json)?;
    let content = fs::read_to_string("test.json")?;
    println!("content:{content}");
    fs::remove_file("test.json")?;
    Ok(())
}
