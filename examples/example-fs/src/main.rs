use std::env;
use tracing::info;
use wang_utils::core::resolve_file_path;
use wang_utils::logger::init_tracing;

fn main() -> anyhow::Result<()> {
    env::set_var("RUST_LOG", "info");
    // 测试logger包
    init_tracing();
    // 测试fs包
    let result = resolve_file_path(".././examples/example-fs")?;
    info!("result:{result:?}");
    Ok(())
}
