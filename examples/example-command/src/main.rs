use std::env;
use tracing::info;
use wang_utils::command::execute_async;
use wang_utils::logger::init_tracing;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    env::set_var("RUST_LOG", "info");
    init_tracing();
    let result = execute_async(
        "pwd && sleep 5 && pwd",
        None,
        |line| {
            info!("stdout:{line}");
        },
        |line| {
            info!("stderr:{line}");
        },
        || {
            info!("complete");
        },
    )
    .await?;
    info!("result:{result:?}");
    Ok(())
}
