//! 日志相关工具
//!
//! ## 示例
//!
//! ```
//!  use wang_utils_logger::init_tracing;
//!
//!  use tracing::info;
//!  use tracing::debug;
//!
//!  fn main() {
//!      // 设置日志级别，默认为ERROR
//!      std::env::set_var("RUST_LOG", "info");
//!      // 测试logger包
//!      init_tracing();
//!      info!("这是info级别的日志");
//!      debug!("这是debug级别的日志");
//!  }
//! ```
#![cfg_attr(docsrs, feature(doc_cfg))]

extern crate self as wang_utils_logger;
mod tracing;
pub use tracing::*;
