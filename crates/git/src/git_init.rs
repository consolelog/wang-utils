use git2::Repository;
use std::path::Path;

pub fn git_init<P: AsRef<Path>>(path: P) -> anyhow::Result<Repository> {
    let repo = Repository::init(path)?;
    Ok(repo)
}
