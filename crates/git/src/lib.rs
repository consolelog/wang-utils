//! git操作相关工具
//!
//! ## 示例
//!
//! ```
//! use git2::Repository;
//! use std::fs;
//! use wang_utils_git::{git_add, git_commit, git_read_commits, git_read_tag, git_status, git_tag};
//!
//! fn main() -> anyhow::Result<()> {
//!     let test_folder  = "~/test_git_folder";
//!     let repo = Repository::open(&test_folder)?;
//!     // 输出文件
//!     fs::write("~/test_git_folder/test.txt", "111")?;
//!     git_add(&repo)?; //git add .
//!     println!("status:{:?}", git_status(&repo)?);
//!     git_commit(&repo, "initial commit")?; //git commit -m "xxx"
//!     println!("status:{:?}", git_status(&repo)?);
//!     println!("commits:{:?}", git_read_commits(&repo)?);
//!     println!("tags:{:?}", git_read_tag(&repo)?);
//!     git_tag(&repo, "v1.2.14")?;
//!     println!("tags:{:?}", git_read_tag(&repo)?);
//!     Ok(())
//! }
//! ```
#![cfg_attr(docsrs, feature(doc_cfg))]

extern crate self as wang_utils_git;
mod git_commit;
mod git_status;
mod git_tag;
mod git_init;

pub use git_commit::*;
pub use git_status::*;
pub use git_tag::*;
pub use git_init::*;

use git2::{Config, IndexAddOption, Repository, Signature};

/// git add .
pub fn git_add(repo: &Repository) -> anyhow::Result<()> {
    let mut index = repo.index()?;
    index.add_all(&["."], IndexAddOption::DEFAULT, None)?;
    index.write()?;
    Ok(())
}

pub fn git_config_get(
    repo: &Repository,
) -> anyhow::Result<(Config, String, String, Signature<'static>)> {
    let config = repo.config()?;
    let user_name = config.get_string("user.name")?;
    let user_email = config.get_string("user.email")?;
    let sig = Signature::now(&user_name, &user_email)?;
    Ok((config, user_name, user_email, sig))
}
