//! 标准库通用工具
#![cfg_attr(docsrs, feature(doc_cfg))]

extern crate self as wang_utils_core;

mod fs;
pub use fs::*;
