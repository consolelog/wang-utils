//! 命令行相关工具
#![cfg_attr(docsrs, feature(doc_cfg))]

extern crate self as wang_utils_command;
mod execute_async;
pub use execute_async::*;
