use crate::MapValue;

pub trait MapValueExtractor<T> {
    fn extract(&self) -> Option<&T>;
}
impl MapValueExtractor<String> for MapValue {
    fn extract(&self) -> Option<&String> {
        match self {
            MapValue::String(s) => Some(s),
            _ => None,
        }
    }
}
impl MapValueExtractor<i16> for MapValue {
    fn extract(&self) -> Option<&i16> {
        match self {
            MapValue::I16(s) => Some(s),
            _ => None,
        }
    }
}
impl MapValueExtractor<u16> for MapValue {
    fn extract(&self) -> Option<&u16> {
        match self {
            MapValue::U16(s) => Some(s),
            _ => None,
        }
    }
}
impl MapValueExtractor<i32> for MapValue {
    fn extract(&self) -> Option<&i32> {
        match self {
            MapValue::I32(s) => Some(s),
            _ => None,
        }
    }
}
impl MapValueExtractor<u32> for MapValue {
    fn extract(&self) -> Option<&u32> {
        match self {
            MapValue::U32(s) => Some(s),
            _ => None,
        }
    }
}
impl MapValueExtractor<f32> for MapValue {
    fn extract(&self) -> Option<&f32> {
        match self {
            MapValue::F32(s) => Some(s),
            _ => None,
        }
    }
}
impl MapValueExtractor<i64> for MapValue {
    fn extract(&self) -> Option<&i64> {
        match self {
            MapValue::I64(s) => Some(s),
            _ => None,
        }
    }
}
impl MapValueExtractor<u64> for MapValue {
    fn extract(&self) -> Option<&u64> {
        match self {
            MapValue::U64(s) => Some(s),
            _ => None,
        }
    }
}
impl MapValueExtractor<f64> for MapValue {
    fn extract(&self) -> Option<&f64> {
        match self {
            MapValue::F64(s) => Some(s),
            _ => None,
        }
    }
}
impl MapValueExtractor<bool> for MapValue {
    fn extract(&self) -> Option<&bool> {
        match self {
            MapValue::Bool(s) => Some(s),
            _ => None,
        }
    }
}
