use crate::MapValue;

impl From<&str> for MapValue {
    fn from(value: &str) -> Self {
        MapValue::String(value.to_string())
    }
}
impl From<String> for MapValue {
    fn from(value: String) -> Self {
        MapValue::String(value)
    }
}
impl From<i16> for MapValue {
    fn from(value: i16) -> Self {
        MapValue::I16(value)
    }
}
impl From<u16> for MapValue {
    fn from(value: u16) -> Self {
        MapValue::U16(value)
    }
}
impl From<i32> for MapValue {
    fn from(value: i32) -> Self {
        MapValue::I32(value)
    }
}
impl From<u32> for MapValue {
    fn from(value: u32) -> Self {
        MapValue::U32(value)
    }
}
impl From<f32> for MapValue {
    fn from(value: f32) -> Self {
        MapValue::F32(value)
    }
}
impl From<i64> for MapValue {
    fn from(value: i64) -> Self {
        MapValue::I64(value)
    }
}
impl From<u64> for MapValue {
    fn from(value: u64) -> Self {
        MapValue::U64(value)
    }
}
impl From<f64> for MapValue {
    fn from(value: f64) -> Self {
        MapValue::F64(value)
    }
}
impl From<bool> for MapValue {
    fn from(value: bool) -> Self {
        MapValue::Bool(value)
    }
}
impl<T> From<Option<T>> for MapValue
where
    T: Into<MapValue>,
{
    fn from(value: Option<T>) -> Self {
        MapValue::OptionValue(value.map(|v| Box::new(v.into())))
    }
}
