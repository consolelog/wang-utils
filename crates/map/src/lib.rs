//! map相关工具
//!
//! # 示例
//! ```
//! use std::collections::HashMap;
//! use wang_utils_map::HashmapCustom;
//!
//! let mut map = HashMap::new();
//! map.set_value("i32", 111);
//! map.set_value("String", "111".to_string());
//! let option = map.get_value::<i32>("i32");
//! assert_eq!(Some(111), option);
//! let option1 = map.get_value_ref::<String>("String");
//! assert_eq!(Some(&"111".to_string()), option1);
//! ```
#![cfg_attr(docsrs, feature(doc_cfg))]

extern crate self as wang_utils_map;

mod extractor;
mod map_struct;
mod setter;
pub use extractor::*;
pub use map_struct::*;
