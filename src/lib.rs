#![doc = include_str!("../README.md")]
#![cfg_attr(docsrs, feature(doc_cfg))]

extern crate self as wang_utils;
/// 标准库通用工具
pub use wang_utils_core as core;
/// 日志相关工具
pub use wang_utils_logger as logger;
/// json相关工具
pub use wang_utils_json as json;

#[cfg(feature = "command")]
#[cfg_attr(docsrs, doc(cfg(feature = "command")))]
#[doc(no_inline)]
/// 命令行相关工具
pub use wang_utils_command as command;

#[cfg(feature = "map")]
#[cfg_attr(docsrs, doc(cfg(feature = "map")))]
#[doc(no_inline)]
/// map相关工具
pub use wang_utils_map as map;

#[cfg(feature = "git")]
#[cfg_attr(docsrs, doc(cfg(feature = "git")))]
#[doc(no_inline)]
/// git相关工具
pub use wang_utils_git as git;
