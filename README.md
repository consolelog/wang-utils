# 个人使用的rust工具库

主要用于简化rust开发

## Feature整理

该库拆分成了多个feature，默认情况下会引用部分feature，如需其他非默认的请自行添加

| 名称      | 是否默认 | 简介          |
|---------|------|-------------|
| full    |      | 包含全部feature |
| core    | √    | 标准库通用工具     |
| logger  | √    | 日志相关工具      |
| command |      | 命令行相关工具     |
| map     |      | map相关工具     |
| git     |      | git相关工具     |
| json    | √    | json相关工具    |
